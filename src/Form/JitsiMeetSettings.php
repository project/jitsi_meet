<?php

namespace Drupal\jitsi_meet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure youmedico_jitsi settings for this site.
 */
class JitsiMeetSettings extends ConfigFormBase
{

    /**
     * Settings.
     *
     * @var string Config settings
     */
    const SETTINGS = 'jitsi_meet.settings';

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'jitsi_meet';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config(static::SETTINGS);

        $form['jitsi_meet_live_meeting_domain'] = [
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('Live Meeting Domain'),
            '#attributes' => ['placeholder' => $this->t('domain')],
            '#default_value' => $config->get('jitsi_meet_live_meeting_domain') ? $config->get('jitsi_meet_live_meeting_domain') : '',
            '#description' => $this->t('Server that manage video connections'),
        ];

        $form['jitsi_meet_live_meeting_provider_name'] = [
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('Provider Name'),
            '#attributes' => ['placeholder' => $this->t('provider_name')],
            '#default_value' => $config->get('jitsi_meet_live_meeting_provider_name') ? $config->get('jitsi_meet_live_meeting_provider_name') : '',
            '#description' => $this->t('Provider Name'),
        ];

        $form['jitsi_meet_live_meeting_app_name'] = [
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('App Name'),
            '#attributes' => ['placeholder' => $this->t('app_name')],
            '#default_value' => $config->get('jitsi_meet_live_meeting_app_name') ? $config->get('jitsi_meet_live_meeting_app_name') : '',
            '#description' => $this->t('App Name'),
        ];

        $form['jitsi_meet_live_meeting_jwt_secret'] = [
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('JWT Secret'),
            '#attributes' => ['placeholder' => $this->t('jwt_secret')],
            '#default_value' => $config->get('jitsi_meet_live_meeting_jwt_secret') ? $config->get('jitsi_meet_live_meeting_jwt_secret') : '',
            '#description' => $this->t('JWT Secret'),
        ];

        $form['jitsi_meet_live_meeting_jwt_iss'] = [
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('JWT iss'),
            '#attributes' => ['placeholder' => $this->t('jwt_iss')],
            '#default_value' => $config->get('jitsi_meet_live_meeting_jwt_iss') ? $config->get('jitsi_meet_live_meeting_jwt_iss') : '',
            '#description' => $this->t('JWT iss'),
        ];

        $form['jitsi_meet_live_meeting_jwt_sub'] = [
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('JWT sub'),
            '#attributes' => ['placeholder' => $this->t('jwt_sub')],
            '#default_value' => $config->get('jitsi_meet_live_meeting_jwt_sub') ? $config->get('jitsi_meet_live_meeting_jwt_sub') : '',
            '#description' => $this->t('JWT sub'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {

        $this->configFactory->getEditable(static::SETTINGS)
            ->set('jitsi_meet_live_meeting_domain', $form_state->getValue('jitsi_meet_live_meeting_domain'))
            ->set('jitsi_meet_live_meeting_provider_name', $form_state->getValue('jitsi_meet_live_meeting_provider_name'))
            ->set('jitsi_meet_live_meeting_app_name', $form_state->getValue('jitsi_meet_live_meeting_app_name'))
            ->set('jitsi_meet_live_meeting_jwt_secret', $form_state->getValue('jitsi_meet_live_meeting_jwt_secret'))
            ->set('jitsi_meet_live_meeting_jwt_iss', $form_state->getValue('jitsi_meet_live_meeting_jwt_iss'))
            ->set('jitsi_meet_live_meeting_jwt_sub', $form_state->getValue('jitsi_meet_live_meeting_jwt_sub'))
            ->save();

        parent::submitForm($form, $form_state);
    }

}
