<?php

/**
 * @file
 * Contains \Drupal\youmedico_doctor\Plugin\Block\DoctorPatientsList.
 */

namespace Drupal\jitsi_meet\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'User' block.
 *
 * @Block(
 *   id = "jitsi_live_meeting_ready",
 *   admin_label = @Translation("Live Meeting Ready"),
 *   category = @Translation("Jitsi Meet Blocks")
 * )
 */

class LiveMeetingReady extends BlockBase implements ContainerFactoryPluginInterface
{
    protected $current_user;
    /**
     * @var \Drupal\Core\Entity\EntityTypeManager
     */
    protected $entityTypeManager;

    /**
     * @var RouteMatchInterface
     */
    private $routeMatch;

    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('current_user'),
            $container->get('entity_type.manager'),
            $container->get('current_route_match')
        );
    }

    public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxy $current_user, EntityTypeManager $entityTypeManager, RouteMatchInterface $routeMatch) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->current_user = $current_user;
        $this->entityTypeManager = $entityTypeManager;
        $this->routeMatch = $routeMatch;
    }
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $uid = $this->current_user->id();

        $logged_in_user = $this->entityTypeManager->getStorage('user')->load($uid);

        $appointment_id = $this->routeMatch->getParameter('appointment_id');

        $meeting_room_id = base_convert ($appointment_id.$uid, 10, 16);

        $build = array(
            '#theme' => 'jitsi_live_meeting_ready',
            '#meeting_room_id' => $meeting_room_id,
        );

        return $build;
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $config = $this->getConfiguration();

        return $form;
    }

    public function getCacheMaxAge() {
        return 0;
    }

}
