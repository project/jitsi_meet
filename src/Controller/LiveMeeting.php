<?php

namespace Drupal\jitsi_meet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LiveMeeting extends ControllerBase
{

  public function ready() {
    return [
      '#markup' => '',
    ];
  }

  public function joined() {
    return [
      '#markup' => '',
    ];
  }

  public function finished() {
    return [
      '#markup' => '',
    ];
  }
}
