<?php

namespace Drupal\jitsi_jwt\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\jitsi_jwt\JitsiJwt\JsonWebTokens;
use Drupal\user\Entity\User;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Jitsi jwt routes.
 */
class JitsiJwtController extends ControllerBase {

  const DEFAULT_FALLBACK_USER_IMAGE = "https://drupal.org/files/EL_blue_RGB%281%29.png";

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * @var EntityRepositoryInterface  $entityManager
   */
  protected $entityManager;

  public function __construct(ConfigFactoryInterface $config_factory,AccountInterface $account, EntityRepositoryInterface $entityManager) {
    $this->configFactory = $config_factory;
    $this->account = $account;
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('entity.repository')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

  public function jwt(Request $request, $roomId, $redirect = TRUE) {
    $config = $this->config('jitsi_jwt.settings');
    $configItems = $config->get();

    /** @var User $account */
    $account = User::load($this->account->id());
    $uuid = $account->uuid();
    if (empty($uuid)) {
      $uuid = "BROKEN-$this->account->id()";
      //TODO encode Domain in this!
    }
    if ($roomId === "/") {
      $roomId = ""; //No room Selected.
    }
    $jwt = new JsonWebTokens($configItems['url'], $configItems['key'], $configItems['jti'], $configItems['aud'], $roomId);
    //    $ret = $this->build();
    //    $ret['content']['#markup'] = $ret['content']['#markup'] . "<br>" . "<pre>roomID: $roomId</pre>" . json_encode($request);
    // what?
    $currentHost = $request->getHost();
    $accountName = $this->account->getAccountName();
    $accountEmail = $this->account->getEmail();
    $accountArray = $account->toArray();
    $accountLink = "";
    // end what?
    if (!empty($accountArray['user_picture'])) {
      //Get user picture link
      $accountLink = file_create_url($account->user_picture->entity->getFileUri());
    }
    else {
      //Get Default User picture Link
      $field = $account->get('user_picture');
      $default_image = $field->getSetting('default_image');
      if (empty($default_image) || empty($default_image['uuid'])) {
        //no default found, using fallback
        $accountLink = self::DEFAULT_FALLBACK_USER_IMAGE;
      }
      else {
        $default_image_file = $this->entityManager->loadEntityByUuid('file', $default_image['uuid']);
        if (empty($default_image_file)) {
          //Default image does not link to anything, using fallback.
          $accountLink = self::DEFAULT_FALLBACK_USER_IMAGE;
        }
        else {
          $accountLink = file_create_url($default_image_file->getFileUri());
        }
      }
    }
    $roles = $this->account->getRoles(TRUE);
    $jwtUrl = $jwt->createToken($uuid . "@" . $currentHost, $uuid, $accountName, $accountEmail, $accountLink, $roles)
      ->getUrl();

    if ($redirect) {
      $response = new TrustedRedirectResponse($jwtUrl, 307);
      $response->setCache([
        //        'etag' => ,
        //        'last_modified',
        'max_age' => 10,
        //      's_maxage',
        'private' => TRUE,
        //      'public',
        //      'immutable'
      ]);

      //    $response->setPrivate();
      //    $response->setImmutable(true);
      //    $response->setClientTtl(10);
      //    $response->setMaxAge(10);
      return $response;
    } else {
      return $jwtUrl;
    }
  }
}
