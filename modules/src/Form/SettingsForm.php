<?php

namespace Drupal\jitsi_jwt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;

/**
 * Configure Jitsi jwt settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  private const MINIMUM_SECRET_HASH_LENGTH = 32;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jitsi_jwt_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jitsi_jwt.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('jitsi_jwt.settings');


    $url = $config->get('url');
    if(empty($url)){
      $newUrl = TRUE;
      $urlPlaceholder =  "https://jitsi.example.com";
    } else {
      $newUrl = FALSE;
      $urlPlaceholder = $url;
    }
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Jitsi Meet URL'),
      '#description' => $this->t('The Domain to send the user to for '
        . 'video chat.'),
      '#size' => 255,
      '#defaultvalue' => $url,
      '#maxlength' => 8192,
      '#required' => $newUrl,
      '#attributes' => [
        'placeholder' => $urlPlaceholder,
      ],
    ];

    $jti = $config->get('jti');
    $form['jti'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JWT ID'),
      '#description' => $this->t('The ID (JTI) to identify the creator of'
        . ' JWT, aka. this instance.'),
      '#default_value' => $jti,
      '#size' => 255,
      '#maxlength' => 8192,
      '#required' => FALSE,
      '#attributes' => [
        'placeholder' => "JWT JTI (an identifier to ID the signing party)",
      ],
    ];

    $aud = $config->get('aud');
    $form['aud'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JWT Audience Tag'),
      '#description' => $this->t('The Audience tag (AUD) of the JWT'),
      '#default_value' => $aud,
      '#size' => 255,
      '#maxlength' => 8192,
      '#required' => FALSE,
      '#attributes' => [
        'placeholder' => "JWT AUD (an identifier to ID the intended audience)",
      ],
    ];

    $key = $config->get('key');
    $form['key'] = [
      '#type' => 'password',
      '#title' => $this->t('JWT Secret'),
      '#description' => $this->t('The secret used to proof we are allowed'
        . 'to sign for the targeted Jitsi.'),
      '#default_value' => $key,
      '#size' => 255,
      '#minlength' => 24,
      '#maxlength' => 8192,
      '#required' => FALSE,
      '#attributes' => [
        'placeholder' => "********************************",
        'autocomplete' => "new-password",
      ],
    ];
    unset($key);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $formValues = $form_state->getValues();
    if(!empty($formValues['url'])) {
      if (
        strlen($formValues['url']) <= 4 ||
        strtolower($formValues['url']) === 'https://jitsi.example.com')
      {
        $form_state->setErrorByName('url', $this->t('Please provide '
          . 'a proper Jitsi Meet URL.'));
      }
      $urlParts = parse_url($formValues['url']);
      $result = Settings::get('trusted_host_patterns', []);
      foreach ($result as $pattern) {
        $test = preg_match("/$pattern/", $urlParts['host']);
        if ($test === 1) {
          $form_state->setErrorByName(
            'url',
            $this->t(
              'For Security reasons we can not accept this Hostname '
               . '(@host) since its valid for this Drupal.<br><sub>Hint: Check '
               . 'your Trusted host settings.</sub>',
              ["@host" => $urlParts['host']]
            )
          );
          break;
        }
      }
    }
    if(!empty($formValues['key'])){
      $keyHash = base64_encode($formValues['key']);
      $hashLength = mb_strlen($keyHash, '8bit');
      $keyLength = mb_strlen($formValues['key'], '8bit');
      if($keyHash % 8 !== 0){
        $form_state->setErrorByName(
          'key',
          $this->t(
            'This key is inconsistent, we can not accepted it. <sub>Key '
             . 'is not aligned to 8bit</sub>'
          )
        );
      } else {
        if($hashLength < self::MINIMUM_SECRET_HASH_LENGTH ){
          $form_state->setErrorByName(
            'key',
            $this->t(
              'This key is (cryptographically) to short.<br><sub>You can add '
              . 'a few more characters to the key to resolve this issue.</sub>'
            )
          );
        } else {
          if($keyLength < self::MINIMUM_SECRET_HASH_LENGTH ){
            $form_state->setErrorByName(
              'key',
              $this->t(
                'This key is (cryptographically) to short.<br><sub>You can add '
                . 'a few more characters to the key to resolve this issue.</sub>'
              )
            );
          }
        }
      }
    }
    // TODO implement better validation for the URL.
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $newConfig = array();
    foreach ($values as $id => $value){
      switch ($id){
        default:
          $newConfig[$id] = $value;
          break;
        case "submit":// FALLTHROUGH
        case "form_build_id":// FALLTHROUGH
        case "form_token":// FALLTHROUGH
        case "form_id":// FALLTHROUGH
        case "op": // FALLTHROUGH
          break; //ignore
      }
    }
    $config = $this->config('jitsi_jwt.settings');
    $updates = 0;
   foreach ($newConfig as $id => $value){
     if($id === "url" && empty($value)){
       continue;// ignore empty value for the url.
     }
     if($id === "key" && empty($value)){
       continue;// ignore empty value for the key.
     }
     $updates++;
     $config->set($id, $value);
   }
   if($updates > 0){
     $config->save();
   }
    parent::submitForm($form, $form_state);
  }
}
