Jitsi jwt package

#### Configuration of the Jitsi Meet URL. 
For the `Jitsi Meet URL` we currently have the (UI) limitation of not allowing
 it to match any of the `trusted host patterns` to prevent (potential) abuse with
 the relay.  

- `web-token/jwt-signature suggests installing web-token/jwt-signature-algorithm-ecdsa` (ECDSA Based Signature Algorithms)
- `web-token/jwt-signature suggests installing web-token/jwt-signature-algorithm-eddsa` (EdDSA Based Signature Algorithms)
- `web-token/jwt-signature suggests installing web-token/jwt-signature-algorithm-hmac` (HMAC Based Signature Algorithms)
- `web-token/jwt-signature suggests installing web-token/jwt-signature-algorithm-none` (None Signature Algorithm)
- `web-token/jwt-signature suggests installing web-token/jwt-signature-algorithm-rsa` (RSA Based Signature Algorithms)
- `web-token/jwt-signature suggests installing web-token/jwt-signature-algorithm-experimental` (Experimental Signature Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-aescbc` (AES CBC Based Content Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-aesgcm` (AES GCM Based Content Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-aesgcmkw` (AES GCM Key Wrapping Based Key Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-aeskw` (AES Key Wrapping Based Key Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-dir` (Direct Key Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-ecdh-es` (ECDH-ES Based Key Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-pbes2` (PBES2 Based Key Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-rsa` (RSA Based Key Encryption Algorithms)
- `web-token/jwt-encryption suggests installing web-token/jwt-encryption-algorithm-experimental` (Experimental Key and Signature Algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-aescbc` (Adds AES-CBC based encryption algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-aesgcm` (Adds AES-GCM based encryption algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-aesgcmkw` (Adds AES-GCM Key Wrapping based encryption algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-aeskw` (Adds AES Key Wrapping based encryption algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-dir` (Adds Direct encryption algorithm)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-ecdh-es` (Adds ECDH-ES based encryption algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-pbes2` (Adds PBES2 based encryption algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-encryption-algorithm-rsa` (Adds RSA based encryption algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-signature-algorithm-ecdsa` (Adds ECDSA based signature algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-signature-algorithm-eddsa` (Adds EdDSA based signature algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-signature-algorithm-none` (Adds none signature algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-signature-algorithm-hmac` (Adds HMAC based signature algorithms)
- `web-token/jwt-easy suggests installing web-token/jwt-signature-algorithm-rsa` (Adds RSA based signature algorithms)